if(BUILD_TESTS)
  enable_testing()
endif(BUILD_TESTS)

function(add_auvaltool_test target name type subtype manufacturer)
  if(BUILD_TESTS)
    set(cmd auvaltool -strict  -v ${type} ${subtype} ${manufacturer})
    add_custom_target(auvaltool_${name} ${cmd})
    add_test(NAME auvaltool::${name} COMMAND ${cmd})
  endif(BUILD_TESTS)
endfunction()

function(add_unit_test target src)
  if(BUILD_TESTS)
    set(properties ${ARGV2})

    if(NOT TARGET ${target})
      add_executable(${target} ${src})
      target_link_libraries(${target} PUBLIC gtest_main filur filur-ep)
      if(APPLE)
        target_link_libraries(${target} PRIVATE "-framework CoreFoundation")
      endif()

      add_test(NAME ${target} COMMAND ${target})
    else()
      target_sources(${target} PRIVATE ${src})
    endif()

    if(properties AND (NOT properties EQUAL ""))
      set_target_properties(${target} PROPERTIES ${properties})
    endif()

  endif(BUILD_TESTS)
endfunction()
