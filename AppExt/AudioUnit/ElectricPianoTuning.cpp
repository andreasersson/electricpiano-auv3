/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ElectricPianoTuning.hpp"

#include <algorithm>

namespace tuning {

void interpolate(const std::vector<TuningKey>& tunings,
                 std::array<key_tuning_t, TUNING_MAX_NUMBER_OF_KEYS>& key_tuning) {
  auto enabled =[](const TuningKey& tuning_key) { return tuning_key.enabled; };

  int last_key = static_cast<int>(key_tuning.size()) - 1;
  int first_key = 0;
  TuningKey next = {.key = last_key, .enabled = true, .timbre = 1.0, .volume = 1.0, .gain = 1.0};
  auto current_it = tunings.end();
  auto next_it = find_if(tunings.begin(), tunings.end(), enabled);
  if (next_it != tunings.end()) {
    next = *next_it;
  }
  TuningKey current = next;
  current.key = first_key;

  while (next_it != tunings.end()) {
    interpolate(current, next, key_tuning);
    current_it = next_it;
    current = next;
    next_it = find_if(next_it + 1, tunings.end(), enabled);
    if (next_it != tunings.end()) {
      next = *next_it;
    }
  }

  if (current.key < last_key) {
    next = current;
    next.key = last_key;
    interpolate(current, next, key_tuning);
  }
}

void interpolate(const TuningKey& begin,
                 const TuningKey& end,
                 std::array<key_tuning_t, TUNING_MAX_NUMBER_OF_KEYS>& key_tuning) {
    double curve = 1.0;
    if (begin.curve >= 0.0) {
      curve = 1.0 + begin.curve;
    } else {
      curve = 1.0 / (1.0 + fabs(begin.curve));
    }

  for (int key = begin.key; key <= end.key; ++key) {
    double k = static_cast<double>(key - begin.key) / (end.key - begin.key);
    double g = std::pow(k, curve);
    double timbre = begin.timbre + (end.timbre - begin.timbre) * g;
    double volume = begin.volume + (end.volume - begin.volume) * g;
    double gain = begin.gain + (end.gain - begin.gain) * g;
    double clang = begin.clang + (end.clang - begin.clang) * g;
    key_tuning.at(key) = { .pickup_timbre = timbre, .pickup_volume = volume, .clang_volume = clang, .gain = gain};
  }
}
}  // namespace tuning