/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "ElectricPianoKernel.hpp"
#import "ElectricPianoParameterAddresses.h"

#import <CoreMIDI/CoreMIDI.h>

static tuning_array default_tuning() {
  std::vector<tuning::TuningKey> tuningkeys = {
    { .key = 0, .enabled = true, .timbre = 1.0, .volume = 1.0, .gain = 1.0, .clang = 1.0, .curve = 0.0 },
    { .key = 60, .enabled = true, .timbre = 1.0, .volume = 1.0, .gain = 1.0, .clang = 1.0, .curve = 0.5 },
    { .key = 127, .enabled = true, .timbre = 0.0, .volume = 0.0, .gain = 1.0, .clang = 1.0, .curve = 0.0 }
  };
  tuning_array tuning;

  tuning::interpolate(tuningkeys, tuning);

  return tuning;
}

ElectricPianoKernel::ElectricPianoKernel() {
  m_tuning = default_tuning();
}


void ElectricPianoKernel::initialize(int channelCount, double inSampleRate) {
  m_sampleRate = inSampleRate;
  ep_parameters_init(&m_ep_parameters);
  ep_init(m_sampleRate, &m_ep);

  ep_set_key_tuning(m_tuning.data(), &m_ep_parameters);

  for (const auto& [address, value] : m_parameters) {
    setParameter(address, value);
  }
}

void ElectricPianoKernel::setMaximumFramesToRender(const AUAudioFrameCount &maxFrames) {
  m_maxFramesToRender = maxFrames;
  m_buffer.resize(m_maxFramesToRender);
}

void ElectricPianoKernel::setParameter(AUParameterAddress address, AUValue value) {
  m_parameters[address] = value;

  switch (address) {
    case ElectricPianoParameterAddress::volume:
      ep_set_volume(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::decay:
      ep_set_decay(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::release:
      ep_set_release(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::velocityCurve:
      ep_set_velocity_curve(value, &m_ep_parameters);
      ep_set_clang_velocity_curve(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::velocityDepth:
      ep_set_velocity_depth(value, &m_ep_parameters);
      ep_set_clang_velocity_depth(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::clangVolume:
      ep_set_clang_volume(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::clangDecay:
      ep_set_clang_decay(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::pickupTimbre:
      ep_set_pickup_timbre(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::pickupVolume:
      ep_set_pickup_volume(value, &m_ep_parameters);
      break;

    case ElectricPianoParameterAddress::updateNoteNumber:
      m_update_note_number = (value >= 0.5) ? true : false;
      break;

    case ElectricPianoParameterAddress::noteNumber:
      // noteNumber is a read only parameter
      break;

    default:
      break;
  }
}

AUValue ElectricPianoKernel::getParameter(AUParameterAddress address) {
  switch (address) {
    default: return m_parameters[address];
  }
}

bool ElectricPianoKernel::modifiedParameter(AUParameterAddress& address, AUValue& value) {
  bool return_value = false;

  if (m_note_number_changed) {
    m_note_number_changed = false;
    return_value = true;
    address = ElectricPianoParameterAddress::noteNumber;
    value = m_parameters[ElectricPianoParameterAddress::noteNumber];
  }

  return return_value;
}

void ElectricPianoKernel::setCustomTuning(const std::vector<tuning::TuningKey>& tuningKeys) {
  tuning_array tuning;

  tuning::interpolate(tuningKeys, tuning);

  // FIXME: This is not thread safe, the kernel migth be reading data from the array when we modify it.
  m_tuning = tuning;
}

bool ElectricPianoKernel::subProcess(AudioBufferList* inBufferList,
                                     AudioBufferList* outBufferList,
                                     AUAudioFrameCount bufferOffset,
                                     AUAudioFrameCount numFramesToProcess,
                                     AUEventSampleTime timestamp) {
  float* leftOut = ((float*)outBufferList->mBuffers[0].mData) + bufferOffset;
  float* rightOut = ((float*)outBufferList->mBuffers[1].mData) + bufferOffset;
  bool isSilent = true;

  if (m_bypassed) {
    for (size_t n = 0u; n < numFramesToProcess; ++n) {
      leftOut[n] = 0.0f;
      rightOut[n] = 0.0f;
    }

    return isSilent;
  }

  for (size_t n = 0u; n < numFramesToProcess; ++n) {
    m_buffer[n] = 0.0f;
  }

  isSilent &= (ep_num_active_voices(&m_ep) == 0);
  ep_process(m_buffer.data(), numFramesToProcess, &m_ep_parameters, &m_ep);
  isSilent &= (ep_num_active_voices(&m_ep) == 0);

  for (size_t n = 0u; n < numFramesToProcess; ++n) {
    float sample = static_cast<float>(m_buffer[n]);
    leftOut[n] = sample;
    rightOut[n] = sample;
  }

  return isSilent;
}

void ElectricPianoKernel::handleEvent(AUEventSampleTime now, AURenderEvent const *event) {
  switch (event->head.eventType) {
    case AURenderEventParameter: {
      handleParameterEvent(now, event->parameter);
      break;
    }

    case AURenderEventMIDIEventList: {
      handleMIDIEventList(now, &event->MIDIEventsList);
      break;
    }

    case AURenderEventMIDI:
      break;

    default:
        break;
  }
}

void ElectricPianoKernel::handleParameterEvent(AUEventSampleTime now, AUParameterEvent const& parameterEvent) {
}

void ElectricPianoKernel::handleMIDIEventList(AUEventSampleTime now, AUMIDIEventList const* midiEvent) {
  auto visitor = [] (void* context, MIDITimeStamp timeStamp, MIDIUniversalMessage message) {
    auto kernel = static_cast<ElectricPianoKernel*>(context);

    switch (message.type) {
      case kMIDIMessageTypeChannelVoice2: {
        kernel->handleMIDI2VoiceMessage(message);
      }
        break;

      default:
        break;
    }
  };

  MIDIEventListForEachEvent(&midiEvent->eventList, visitor, this);
}

void ElectricPianoKernel::handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message) {
  static const double normalize32 = 1.0 / double(std::numeric_limits<uint32_t>::max());
  static const double normalize16 = 1.0 / double(std::numeric_limits<uint16_t>::max());

  const auto& note = message.channelVoice2.note;

  switch (message.channelVoice2.status) {
    case kMIDICVStatusNoteOff:
      ep_note_off(note.number, &m_ep_parameters, &m_ep);
      break;

    case kMIDICVStatusNoteOn:
      ep_note_on(note.number, (double)message.channelVoice2.note.velocity * normalize16, &m_ep_parameters, &m_ep);
      if (m_update_note_number) {
        m_parameters[ElectricPianoParameterAddress::noteNumber] = static_cast<AUValue>(note.number);
        m_note_number_changed = true;
      }
      break;

    case kMIDICVStatusControlChange:
      switch (message.channelVoice2.controlChange.index) {
        case MidiCCDamperPedal:
          ep_sustain_pedal(message.channelVoice2.controlChange.data *normalize32 >= 0.5 ? pedal_on : pedal_off, &m_ep_parameters, &m_ep);
          break;

        case MidiCCSostenutoPedal:
          ep_sostenuto_pedal(message.channelVoice2.controlChange.data *normalize32 >= 0.5 ? pedal_on : pedal_off, &m_ep_parameters, &m_ep);
          break;

        case MidiCCSoftPedal:
          ep_soft_pedal(message.channelVoice2.controlChange.data *normalize32 >= 0.5 ? pedal_on : pedal_off, &m_ep_parameters, &m_ep);
          break;

        case MidiCCAllSoundOff:
        case MidiCCAllNotesOff:
          ep_all_notes_off(&m_ep_parameters, &m_ep);
          break;

        default:
          break;
      }
      break;

    default:
        break;
  }
}
