/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EP_TUNING_H
#define EP_TUNING_H 0

#include <filur/ep/tuning.h>

#include <array>
#include <vector>

namespace tuning {

struct TuningKey {
  int key = 0;
  bool enabled = true;
  double timbre = 1.0;
  double volume = 1.0;
  double gain = 1.0;
  double clang = 1.0;
  double curve = 0.0;
};

void interpolate(const std::vector<TuningKey>& tunings,
                 std::array<key_tuning_t, TUNING_MAX_NUMBER_OF_KEYS>& key_tuning);
void interpolate(const TuningKey& begin,
                 const TuningKey& end,
                 std::array<key_tuning_t, TUNING_MAX_NUMBER_OF_KEYS>& key_tuning);

}  // namespace tuning

#endif  // EP_TUNING_H
