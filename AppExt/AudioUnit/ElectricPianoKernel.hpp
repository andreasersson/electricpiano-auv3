/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELECTRIC_PIANO_KERNEL_HPP
#define ELECTRIC_PIANO_KERNEL_HPP 0

#import "KernelBase.hpp"
#import "ElectricPianoTuning.hpp"

#import <AudioToolbox/AudioToolbox.h>

#import <vector>
#import <map>

#import <filur/ep/ep.h>
#import <filur/ep/ep_parameters.h>
#import <filur/ep/tuning.h>

typedef std::array<key_tuning_t, TUNING_MAX_NUMBER_OF_KEYS> tuning_array;

class ElectricPianoKernel : public KernelBase {
public:
  ElectricPianoKernel();
  virtual ~ElectricPianoKernel() {}

  virtual void initialize(int channelCount, double inSampleRate);
  virtual void deInitialize() {}

  virtual bool isBypassed() {
    return m_bypassed;
  }

  virtual void setBypass(bool shouldBypass) {
    m_bypassed = shouldBypass;
  }

  virtual AUAudioFrameCount maximumFramesToRender() const {
    return m_maxFramesToRender;
  }

  virtual void setMaximumFramesToRender(const AUAudioFrameCount &maxFrames);

  virtual void setMusicalContextBlock(AUHostMusicalContextBlock contextBlock) {
    m_musicalContextBlock = contextBlock;
  }

  virtual MIDIProtocolID AudioUnitMIDIProtocol() const {
    return kMIDIProtocol_2_0;
  }

  virtual void setParameter(AUParameterAddress address, AUValue value);
  virtual AUValue getParameter(AUParameterAddress address);
  virtual bool modifiedParameter(AUParameterAddress& address, AUValue& value);

  void setCustomTuning(const std::vector<tuning::TuningKey>& tuningKey);

protected:
  virtual bool subProcess(AudioBufferList* inBufferList,
                          AudioBufferList* outBufferList,
                          AUAudioFrameCount bufferOffset,
                          AUAudioFrameCount numFramesToProcess,
                          AUEventSampleTime timestamp);
  virtual void handleEvent(AUEventSampleTime now, AURenderEvent const *event);
  void handleParameterEvent(AUEventSampleTime now, AUParameterEvent const& parameterEvent);
  void handleMIDIEventList(AUEventSampleTime now, AUMIDIEventList const* midiEvent);
  void handleMIDI2VoiceMessage(const struct MIDIUniversalMessage& message);

private:
  enum {
    MidiCCDamperPedal = 64,
    MidiCCSostenutoPedal = 66,
    MidiCCSoftPedal = 67,
    MidiCCAllSoundOff = 120,
    MidiCCAllNotesOff = 123,
  };

  AUHostMusicalContextBlock m_musicalContextBlock;

  double m_sampleRate = 44100.0;
  std::map<uint64_t, double> m_parameters;

  bool m_bypassed = false;
  AUAudioFrameCount m_maxFramesToRender = 1024;

  ep_parameters_t m_ep_parameters;
  ep_t m_ep;
  std::vector<double> m_buffer;

  tuning_array m_tuning;

  bool m_update_note_number = false;
  bool m_note_number_changed = false;
};

#endif // ELECTRIC_PIANO_KERNEL_HPP
