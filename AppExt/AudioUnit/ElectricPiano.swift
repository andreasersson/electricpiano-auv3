/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Foundation
import AudioToolbox
import AVFoundation
import CoreAudioKit

typealias Parameters = ElectricPianoParameterAddress

extension AUParameter {
  var includeInPresets: Bool {
    return !self.flags.contains(AudioUnitParameterOptions.flag_OmitFromPresets)
  }
}

let kAUPresetParametersKey = "parameters"
let kAUPresetTuningKey = "tuning"

public class ElectricPiano: ElectricPianoAudioUnit {

  var tuning: Tuning?
  let presets: FactoryPresets = FactoryPresets()

  override init(componentDescription: AudioComponentDescription, options: AudioComponentInstantiationOptions) throws {
    try super.init(componentDescription: componentDescription, options: options)
    setupParameterTree(createParameterTree())

    let noteParameter = self.parameterTree?.parameter(withAddress: Parameters.noteNumber.rawValue)
    self.tuning = Tuning(messageChannel: self.messageChannel(for: "tuning"), noteNumberParameter: noteParameter)

    // Load the first factory preset.
    let preset: AUAudioUnitPreset = AUAudioUnitPreset()
    preset.number = 0
    self.currentPreset = preset
  }

  private var _currentPreset: AUAudioUnitPreset?

  private static let parameterPriorityList: [NSNumber] = [
    NSNumber(value: ElectricPianoParameterAddress.pickupTimbre.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.pickupVolume.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.volume.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.clangVolume.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.clangDecay.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.clangDecay.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.velocityCurve.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.velocityCurve.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.velocityDepth.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.release.rawValue),
    NSNumber(value: ElectricPianoParameterAddress.decay.rawValue)
  ]

  public override func parametersForOverview(withCount count: Int) -> [NSNumber] {
    return Array(ElectricPiano.parameterPriorityList[0..<min(count, ElectricPiano.parameterPriorityList.count)])
  }

  public override var factoryPresets: [AUAudioUnitPreset]? {
    return self.presets.factoryPresets()
  }

  public override var currentPreset: AUAudioUnitPreset? {
    get { return _currentPreset }
    set {
      guard let preset = newValue else {
          _currentPreset = nil
          return
      }

      // Factory preset numbers are always >= 0.
      if preset.number >= 0 {
        if let state = self.presets.state(number: preset.number) {
            self.fullState = state
            _currentPreset = preset
        }
      } else {
        do {
          fullStateForDocument = try presetState(for: preset)
          _currentPreset = preset
        } catch {}
      }
    }
  }

  public override var supportsUserPresets: Bool {
      return true
  }

  override public var fullState: [String: Any]? {
    get {
      var state = [String: Any]()
      state[kAUPresetManufacturerKey] = self.componentDescription.componentManufacturer
      state[kAUPresetTypeKey] = self.componentDescription.componentType
      state[kAUPresetSubtypeKey] = self.componentDescription.componentSubType
      state[kAUPresetVersionKey] = self.componentVersion

      if let parameterTree = self.parameterTree {
        var parameters: [String: Any] = [:]
        for parameter in parameterTree.allParameters where parameter.includeInPresets {
            parameters[String(parameter.address)] = parameter.value
        }
        state[kAUPresetParametersKey] = parameters
      }

      if let tuning = self.tuning {
        state[kAUPresetTuningKey] = tuning.getData()
      }

      return state
    }
    set {
      guard let dictionary = newValue else { return }

      if let parameters = dictionary[kAUPresetParametersKey] as? [String: Any] {
        for (addressString, anyValue) in parameters {
          if let address = UInt64(addressString), let value = anyValue as? AUValue {
            if let parameter = self.parameterTree?.parameter(withAddress: address) {
              parameter.value = value
            }
          }
        }
      }

      if let tuningArray = dictionary[kAUPresetTuningKey] as? [[String: NSNumber]] {
        if let tuning = self.tuning {
          tuning.setData(tuning: tuningArray)
        }
      }
    }
  }

// swiftlint:disable function_body_length
  func createParameterTree() -> AUParameterTree {
    // Global
    var globalParameters: [AUParameterNode] = []
    globalParameters.append(
      AUParameterTree.createParameter(
      identifier: "volume",
      name: "volume",
      address: ElectricPianoParameterAddress.volume.rawValue,
      range: -40.0...0.0,
      defaultValue: -16.0,
      unit: .decibels)
    )

    globalParameters.append(
      AUParameterTree.createParameter(
      identifier: "velocityCurve",
      name: "velocity curve",
      address: ElectricPianoParameterAddress.velocityCurve.rawValue,
      range: -1.0...1.0,
      defaultValue: 0.0)
    )

    globalParameters.append(
      AUParameterTree.createParameter(
      identifier: "velocityDepth",
      name: "velocity depth",
      address: ElectricPianoParameterAddress.velocityDepth.rawValue,
      range: 0...1.0,
      defaultValue: 1.0)
    )

    let global = AUParameterTree.createGroup(withIdentifier: "global", name: "global", children: globalParameters)

    // tonebar
    var tonebarParameters: [AUParameterNode] = []
    tonebarParameters.append(
      AUParameterTree.createParameter(
      identifier: "decay",
      name: "decay",
      address: ElectricPianoParameterAddress.decay.rawValue,
      range: 10e-3...10.0,
      defaultValue: 10.0,
      unit: .seconds)
    )

    tonebarParameters.append(
      AUParameterTree.createParameter(
      identifier: "release",
      name: "release",
      address: ElectricPianoParameterAddress.release.rawValue,
      range: 70e-3...4.0,
      defaultValue: 70.0e-3,
      unit: .seconds)
    )

    let tonebar = AUParameterTree.createGroup(withIdentifier: "tonebar", name: "tonebar", children: tonebarParameters)

    // clang
    let clangVolume = AUParameterTree.createParameter(
      identifier: "clangVolume",
      name: "volume",
      address: ElectricPianoParameterAddress.clangVolume.rawValue,
      range: -40.0...12.0,
      defaultValue: 0.0,
      unit: .decibels
    )

    let clangDecay = AUParameterTree.createParameter(
      identifier: "clangDecay",
      name: "decay",
      address: ElectricPianoParameterAddress.clangDecay.rawValue,
      range: 10e-3...2.0,
      defaultValue: 0.75,
      unit: .seconds
    )

    let clang = AUParameterTree.createGroup(withIdentifier: "clang", name: "clang", children: [clangVolume, clangDecay])

    // pickup
    let pickupTimbre = AUParameterTree.createParameter(
      identifier: "pickupTimbre",
      name: "timbre",
      address: ElectricPianoParameterAddress.pickupTimbre.rawValue,
      range: 0.0...1.0,
      defaultValue: 0.4
    )

    let pickupVolume = AUParameterTree.createParameter(
      identifier: "pickupVolume",
      name: "volume",
      address: ElectricPianoParameterAddress.pickupVolume.rawValue,
      range: 0.0...1.0,
      defaultValue: 0.4
    )

    let pickup = AUParameterTree.createGroup(
      withIdentifier: "pickup",
      name: "pickup",
      children: [pickupTimbre, pickupVolume]
    )

    // note number
    let updateNoteNumber = AUParameterTree.createParameter(
      identifier: "updateNoteNumber",
      name: "update note number",
      address: ElectricPianoParameterAddress.updateNoteNumber.rawValue,
      defaultValue: 0.0,
      strings: ["off", "on"],
      options: [.flag_ExpertMode, .flag_OmitFromPresets]
    )

    let noteNumber = AUParameterTree.createParameter(
      identifier: "noteNumber",
      name: "note number",
      address: ElectricPianoParameterAddress.noteNumber.rawValue,
      range: 0...127,
      defaultValue: 36,
      options: [.flag_MeterReadOnly, .flag_OmitFromPresets]
    )

    let noteNumberGroup = AUParameterTree.createGroup(
      withIdentifier: "notenumber",
      name: "note number",
      children: [updateNoteNumber, noteNumber]
    )

    return AUParameterTree.createTree(withChildren: [global, tonebar, clang, pickup, noteNumberGroup])
  }
  // swiftlint:enable function_body_length
}
