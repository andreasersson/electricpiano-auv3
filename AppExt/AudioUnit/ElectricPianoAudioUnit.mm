/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#import "ElectricPianoAudioUnit.h"

#import "ElectricPianoKernel.hpp"
#import "ElectricPianoTuning.hpp"

#import <AVFoundation/AVFoundation.h>
#import <CoreAudioKit/AUViewController.h>

@interface ElectricPianoAudioUnit ()
@end

@implementation ElectricPianoAudioUnit {
}

- (instancetype)initWithComponentDescription:(AudioComponentDescription)componentDescription options:(AudioComponentInstantiationOptions)options error:(NSError **)outError {
  self = [super initWithKernel:std::make_unique<ElectricPianoKernel>() componentDescription: componentDescription options:options error:outError];

  return self;
}

- (id<AUMessageChannel>)messageChannelFor:(NSString *)channelName {
  if ([channelName isEqualToString: @("tuning") ]) {
    return self;
  } else {
    return [super messageChannelFor:channelName];
  }
}

- (NSDictionary *)callAudioUnit:(NSDictionary *)message {
  id tuning = message[@"tuning"];
  if ((tuning != nil) && [tuning isKindOfClass:[NSArray class]]) {
    [self setTuning: tuning];
  }

  NSDictionary* dict = @{};
  return dict;
}

- (void)setTuning:(NSArray*)tuning {
  std::vector<tuning::TuningKey> tuningKeys;
  for (id object in tuning) {
    if ([object isKindOfClass:[NSDictionary class]]) {
      tuningKeys.push_back([self dictionaryToTuning: object]);
    }
  }
  static_cast<ElectricPianoKernel*>([super kernel])->setCustomTuning(tuningKeys);
}

- (tuning::TuningKey)dictionaryToTuning:(NSDictionary*)tuningDictionary {
  tuning::TuningKey tuningKey;
  for (NSString *key in tuningDictionary) {
    id value = tuningDictionary[key];
    if ([key isEqualToString:@"key"]) {
      tuningKey.key = [value intValue];
    } else if ([key isEqualToString:@"enabled"]) {
      tuningKey.enabled = [value boolValue];
    } else if ([key isEqualToString:@"timbre"]) {
      tuningKey.timbre = [value doubleValue];
    } else if ([key isEqualToString:@"volume"]) {
      tuningKey.volume = [value doubleValue];
    } else if ([key isEqualToString:@"gain"]) {
      tuningKey.gain = [value doubleValue];
    } else if ([key isEqualToString:@"clang"]) {
      tuningKey.clang = [value doubleValue];
    } else if ([key isEqualToString:@"curve"]) {
      tuningKey.curve = [value doubleValue];
    }
  }

  return tuningKey;
}

@end