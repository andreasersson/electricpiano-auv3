/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Foundation
import AudioToolbox
import AVFoundation
import CoreAudioKit

enum ElectricPianoTuningParameterAddress: UInt64 {
  case edit = 1000
  case key
  case enabled
  case timbre
  case volume
  case gain
  case clang
  case curve
}

class TuningKey {
  var key: NSNumber = 36
  var enabled: NSNumber = false
  var timbre: NSNumber = 1.0
  var volume: NSNumber = 1.0
  var gain: NSNumber = 1.0
  var clang: NSNumber = 1.0
  var curve: NSNumber = 0.0

  init(key: NSNumber) {
    self.key = key
  }

  func get() -> [String: NSNumber] {
    return [
      "key": self.key,
      "enabled": self.enabled,
      "timbre": self.timbre,
      "volume": self.volume,
      "gain": self.gain,
      "clang": self.clang,
      "curve": self.curve]
  }

  func set(data: [String: NSNumber]) {
    self.key = data["key", default: 0]
    self.enabled = data["enabled", default: 0.0]
    self.timbre = data["timbre", default: 1.0]
    self.volume = data["volume", default: 1.0]
    self.gain = data["gain", default: 1.0]
    self.clang = data["clang", default: 1.0]
    self.curve = data["curve", default: 0.0]
  }

  func clear() {
    self.enabled = false
    self.timbre = 1.0
    self.volume = 1.0
    self.gain = 1.0
    self.clang = 1.0
    self.curve = 0.0
  }
}

class Tuning: ObservableObject {
  var data: [TuningKey] = []

  var messageChannel: AUMessageChannel?

  var parameterTree: AUParameterTree?
  var observableAUParameterTree: ObservableAUParameterGroup?
  var currentKey = 36

  var fileTypes: [UTType] = [.json]

  private var observerToken: AUParameterObserverToken!

  private let keyRange = 0..<128

  init(messageChannel: AUMessageChannel?, noteNumberParameter: AUParameter?) {
    for key in keyRange {
      data.append(TuningKey(key: NSNumber(value: key)))
    }

    if let auPresetType = UTType(filenameExtension: "aupreset", conformingTo: .propertyList) {
      fileTypes.append(auPresetType)
    }

    self.messageChannel = messageChannel

    self.parameterTree = self.createParameterTree()
    if let parameterTree = self.parameterTree {
      self.observableAUParameterTree = ObservableAUParameterGroup(parameterTree)
      parameterTree.implementorValueObserver = self.valueObserver
      parameterTree.implementorValueProvider = self.valueProvider
    }

    if let parameter = noteNumberParameter {
      self.observerToken = parameter.token { (_ address: AUParameterAddress, _ value: AUValue) in
        DispatchQueue.main.async {
            self.setParameter(address: ElectricPianoTuningParameterAddress.key.rawValue, value: value)
        }
      }
    }
  }

  func valueObserver(parameter: AUParameter, value: AUValue) {
    let valueChanged = self.setValue(address: parameter.address, value: value)

    if (ElectricPianoTuningParameterAddress.key.rawValue == parameter.address) && valueChanged {
      self.updateParameter(address: ElectricPianoTuningParameterAddress.enabled.rawValue)
      self.updateParameter(address: ElectricPianoTuningParameterAddress.timbre.rawValue)
      self.updateParameter(address: ElectricPianoTuningParameterAddress.volume.rawValue)
      self.updateParameter(address: ElectricPianoTuningParameterAddress.gain.rawValue)
      self.updateParameter(address: ElectricPianoTuningParameterAddress.clang.rawValue)
      self.updateParameter(address: ElectricPianoTuningParameterAddress.curve.rawValue)
    }
  }

  func valueProvider(parameter: AUParameter) -> AUValue {
    return self.getValue(address: parameter.address)
  }

  // Load data form file
  func load(fileURL: URL) {
    var tuning: [Any]?

    do {
      let data = try Data(contentsOf: fileURL, options: .mappedIfSafe)
      if fileURL.pathExtension == "json" {
        let json = try? JSONSerialization.jsonObject(with: data)
        tuning = json as? [Any]
      } else if fileURL.pathExtension == "aupreset" {
        if let propertyList = try? PropertyListSerialization.propertyList(
          from: data,
          options: [],
          format: nil) as? [String: Any] {
          tuning = propertyList["tuning"] as? [Any]
        }
      }
    } catch {}

    if tuning != nil {
      self.loadArray(array: tuning!)
      self.updateAudioUnit()
    }
  }

  func getData() -> [[String: NSNumber]] {
    var tuning: [[String: NSNumber]] = []
    for item in self.data where item.enabled.boolValue {
      tuning.append(item.get())
    }

    return tuning
  }

  func setData(tuning: [[String: NSNumber]]) {
    for item in tuning {
      let key = item["key", default: -1]
      if self.keyRange.contains(key.intValue) {
        self.data[key.intValue].set(data: item)
      }
    }

    self.updateAudioUnit()
  }

  private func getValue(address: AUParameterAddress) -> AUValue {
    var value = NSNumber(value: 0.0)

    switch address {
      case ElectricPianoTuningParameterAddress.key.rawValue:
        value = NSNumber(value: self.currentKey)

      case ElectricPianoTuningParameterAddress.enabled.rawValue:
        value = self.data[self.currentKey].enabled

      case ElectricPianoTuningParameterAddress.timbre.rawValue:
        value = self.data[self.currentKey].timbre

      case ElectricPianoTuningParameterAddress.volume.rawValue:
        value = self.data[self.currentKey].volume

      case ElectricPianoTuningParameterAddress.gain.rawValue:
        value = self.data[self.currentKey].gain

      case ElectricPianoTuningParameterAddress.clang.rawValue:
        value = self.data[self.currentKey].clang

      case ElectricPianoTuningParameterAddress.curve.rawValue:
        value = self.data[self.currentKey].curve

      default:
        value = 0.0
      }

    return value.floatValue
  }

  private func setValue(address: AUParameterAddress, value: AUValue) -> Bool {
    var valueChanged: Bool = false
    var updateAudioUnit: Bool = false
    let nsValue = NSNumber(value: value)

    switch address {
      case ElectricPianoTuningParameterAddress.key.rawValue:
        valueChanged = self.currentKey != nsValue.intValue && self.keyRange.contains(nsValue.intValue)
        self.currentKey = nsValue.intValue

      case ElectricPianoTuningParameterAddress.enabled.rawValue:
        updateAudioUnit = self.data[self.currentKey].enabled != nsValue
        self.data[self.currentKey].enabled = nsValue

      case ElectricPianoTuningParameterAddress.timbre.rawValue:
        updateAudioUnit = self.data[self.currentKey].timbre != nsValue // {
        self.data[self.currentKey].timbre = nsValue

      case ElectricPianoTuningParameterAddress.volume.rawValue:
        updateAudioUnit = self.data[self.currentKey].volume != nsValue
        self.data[self.currentKey].volume = nsValue

      case ElectricPianoTuningParameterAddress.gain.rawValue:
        updateAudioUnit = self.data[self.currentKey].gain != nsValue
        self.data[self.currentKey].gain = nsValue

      case ElectricPianoTuningParameterAddress.clang.rawValue:
        updateAudioUnit = self.data[self.currentKey].clang != nsValue
        self.data[self.currentKey].clang = nsValue

      case ElectricPianoTuningParameterAddress.curve.rawValue:
        updateAudioUnit = self.data[self.currentKey].curve != nsValue
        self.data[self.currentKey].curve = nsValue

      default:
        break
      }

    if updateAudioUnit {
      self.updateAudioUnit()
    }

    return valueChanged || updateAudioUnit
  }

  private func setParameter(address: AUParameterAddress, value: AUValue) {
    self.parameterTree?.parameter(withAddress: address)?.value = value
  }

  private func updateParameter(address: AUParameterAddress) {
    self.parameterTree?.parameter(withAddress: address)?.value = self.getValue(address: address)
  }

  private func loadArray(array: [Any]) {
    let newData: [TuningKey] = self.data
    for item in newData {
      item.clear()
    }

    for object in array {
      if let tuning = object as? [String: NSNumber] {
        let key = tuning["key", default: -1]
        if self.keyRange.contains(key.intValue) {
          newData[key.intValue].set(data: tuning)
        }
      }
    }

    self.data = newData
  }

    private func updateAudioUnit() {
      if let callAudioUnit = messageChannel?.callAudioUnit {
        let message: [AnyHashable: Any] = ["tuning": self.getData()]
        _ = callAudioUnit(message)
      }
    }

// swiftlint:disable function_body_length
  private func createParameterTree() -> AUParameterTree {
    var parameters: [AUParameterNode] = []
    parameters.append(AUParameterTree.createParameter(
      identifier: "edit",
      name: "edit",
      address: ElectricPianoTuningParameterAddress.edit.rawValue,
      defaultValue: 0.0,
      strings: ["off", "on"])
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "key",
      name: "key",
      address: ElectricPianoTuningParameterAddress.key.rawValue,
      range: 0...127,
      defaultValue: self.currentKey)
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "enabled",
      name: "enabled",
      address: ElectricPianoTuningParameterAddress.enabled.rawValue,
      defaultValue: 0.0,
      strings: ["disabled", "enabled"])
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "timbre",
      name: "timbre",
      address: ElectricPianoTuningParameterAddress.timbre.rawValue,
      range: 0.0...1.0,
      defaultValue: 1.0)
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "volume",
      name: "volume",
      address: ElectricPianoTuningParameterAddress.volume.rawValue,
      range: 0.0...1.0,
      defaultValue: 1.0)
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "gain",
      name: "gain",
      address: ElectricPianoTuningParameterAddress.gain.rawValue,
      range: 0.0...1.0,
      defaultValue: 1.0)
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "clang",
      name: "clang",
      address: ElectricPianoTuningParameterAddress.clang.rawValue,
      range: 0.0...1.0,
      defaultValue: 1.0)
    )

    parameters.append(AUParameterTree.createParameter(
      identifier: "curve",
      name: "curve",
      address: ElectricPianoTuningParameterAddress.curve.rawValue,
      range: -1.0...1.0,
      defaultValue: 0.0)
    )

    let tuning = AUParameterTree.createGroup(
      withIdentifier: "tuning",
      name: "tuning",
      children: parameters)

    return AUParameterTree.createTree(withChildren: [tuning])
  }
  // swiftlint:enable function_body_length
}
