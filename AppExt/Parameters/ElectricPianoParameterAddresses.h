/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELECTRIC_PIANO_PARAMETER_ADDRESSES_H
#define ELECTRIC_PIANO_PARAMETER_ADDRESSES_H 0

#include <AudioToolbox/AUParameters.h>

#ifdef __cplusplus
namespace ElectricPianoParameterAddress {
#endif

typedef NS_ENUM(AUParameterAddress, ElectricPianoParameterAddress) {
  volume = 0,

  // tonebar
  decay = 10,
  release,
  velocityCurve,
  velocityDepth,

  // clang
  clangVolume = 20,
  clangDecay,

  // pickup
  pickupTimbre = 30,
  pickupVolume,

  // note number
  updateNoteNumber = 100, // If set, noteNumber will be set to the most resent received midi note number.
  noteNumber, // If updateNoteNumber is set, noteNumber will be set to the most resent received midi note number.
};

#ifdef __cplusplus
}
#endif

#endif // ELECTRIC_PIANO_PARAMETER_ADDRESSES_H