/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

func valueToNoteName(value: AUValue) -> String {
  struct NoteNames {
    static let noteName: [String] = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
  }
  let octave = (value / 12).rounded(.towardZero)
  let note = Int(min(11, max(0, value - 12 * octave)))

  return "\(NoteNames.noteName[Int(note)])\(Int(octave - 1))"
}

struct LabelButtonView: View {
  @Environment(\.isEnabled) var isEnabled
  @Environment(\.isDisplayNameHidden) var isDisplayNameHidden
  var text: String = ""
  var systemImage: String = ""
  @Environment(\.isParameterViewCompact) var compact

  var body: some View {
    VStack(spacing: 0) {
      Spacer().frame(maxHeight: self.compact ? 0 : nil)
      Label(self.text, systemImage: systemImage)
        .frame(maxWidth: .infinity, alignment: .center)
        .padding(EdgeInsets(top: 1, leading: 1, bottom: 1, trailing: 1))
        .background(Color.controlColor)
        .cornerRadius(5)
      Spacer().frame(maxHeight: self.compact ? 0 : nil)
      Text(" ")
        .frame(maxWidth: .infinity, alignment: .bottom)
        .padding(0)
        .hidden()
    }
  }
}

struct ElectricPianoTuningView: View {
  var parameterTree: ObservableAUParameterGroup
  var tuning: Tuning

  @State private var showFileImporter = false

  var body: some View {
    VStack(spacing: 0) {
      HStack {
        LabelButtonView(text: "load", systemImage: "arrow.down.doc")
          .compact()
          .onTapGesture {
            _ = fileDialogDefaultDirectory(URL.homeDirectory)
            showFileImporter = true
          }
          .fileImporter(isPresented: $showFileImporter, allowedContentTypes: tuning.fileTypes) { result in
            switch result {
              case .success(let fileUrl):
                self.tuning.load(fileURL: fileUrl)

              case .failure:
                break
              }
            }

        ParameterView(parameter: parameterTree.notenumber.updateNoteNumber)
          .toggle()
          .displayNameHidden()
          .parameterStyle(ParameterLabelButtonStyle(text: "edit", systemImage: "pianokeys", compact: true))
        Rectangle().frame(maxHeight: 5).hidden()
        Rectangle().frame(maxHeight: 5).hidden()
        Rectangle().frame(maxHeight: 5).hidden()
      }

      HStack {
        ParameterViewer(parameter: tuning.observableAUParameterTree!.tuning.key)
          .parameterStyle(ParameterViewerDefaultStyle(prefix: "key: ", valueToString: valueToNoteName))
          .displayNameHidden()
          .compact()
          .modifier(ParameterDisabler(parameter: parameterTree.notenumber.updateNoteNumber, range: 0.0..<1.0))
        ParameterTextFieldView(parameter: tuning.observableAUParameterTree!.tuning.key)
          .displayNameHidden()
          .compact()
          .modifier(ParameterDisabler(parameter: parameterTree.notenumber.updateNoteNumber, range: 0.0..<1.0))
        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.enabled)
          .toggle()
          .displayNameHidden()
          .compact()
          .parameterStyle(ParameterSwitchButtonStyle(size: 5, alignment: .leading))
          .modifier(ParameterDisabler(parameter: parameterTree.notenumber.updateNoteNumber, range: 0.0..<1.0))
          .frame(alignment: .leading)

        Rectangle().frame(maxHeight: 5).hidden()
        Rectangle().frame(maxHeight: 5).hidden()
      }

      HStack {
        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.curve)
          .parameterStyle(ParameterSymmetricKnobStyle())

        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.timbre)
          .parameterStyle(ParameterKnobStyle())

        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.volume)
          .parameterStyle(ParameterKnobStyle())

        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.gain)
          .parameterStyle(ParameterKnobStyle())

        ParameterView(parameter: tuning.observableAUParameterTree!.tuning.clang)
          .parameterStyle(ParameterKnobStyle())
      }
        .modifier(ParameterDisabler(parameter: parameterTree.notenumber.updateNoteNumber, range: 0.0..<1.0))
        .modifier(ParameterDisabler(parameter: tuning.observableAUParameterTree!.tuning.enabled, range: 0.0..<1.0))

      Rectangle().hidden()
    }
      .background(Color.controlBackgroundColor)
  }
}
