/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import SwiftUI

struct DividerGroupBoxStyle: GroupBoxStyle {
  func makeBody(configuration: Configuration) -> some View {
    VStack(spacing: 0) {
      Spacer()
      configuration.label
      Divider()
        .background(Color.windowBackgroundColor)
      Spacer()
      configuration.content
        .padding(5)
      Spacer()
    }
  }
}

struct NoDividerGroupBoxStyle: GroupBoxStyle {
  func makeBody(configuration: Configuration) -> some View {
    VStack(spacing: 0) {
      Spacer()
      configuration.label
      Spacer()
      configuration.content
        .padding(5)
      Spacer()
    }
  }
}

private let _fontSizeScale: Double = 0.05

struct ElectricPianoMainView: View {
  var parameterTree: ObservableAUParameterGroup
  var tuningView: ElectricPianoTuningView?
  let curve: Float = 1.0/3.0

  @State private var selection = 0

  static let widthInUnits = 4
  static let heightInUnits = 2

  init(parameterTree: ObservableAUParameterGroup, tuning: Tuning) {
      self.parameterTree = parameterTree
      self.tuningView = ElectricPianoTuningView(parameterTree: parameterTree, tuning: tuning)
  }

  var body: some View {
    GeometryReader { geometry in
      VStack {
        TabBarView(selection: $selection) {
            Label("electric piano", systemImage: "pianokeys")
            Label("tuning", systemImage: "tuningfork")
        }

        TabView(selection: $selection) {
          HStack {
            GroupBox(label: Text("tonebar")) {
              VStack {
                ParameterView(parameter: parameterTree.tonebar.decay)
                  .parameterStyle(ParameterKnobStyle())
                  .curve(curve)
                ParameterView(parameter: parameterTree.tonebar.release)
                  .parameterStyle(ParameterKnobStyle())
                  .curve(curve)
              }
            }
            GroupBox(label: Text("clang")) {
              VStack {
                ParameterView(parameter: parameterTree.clang.clangDecay)
                  .parameterStyle(ParameterKnobStyle())
                  .curve(curve)
                ParameterView(parameter: parameterTree.clang.clangVolume)
                  .parameterStyle(ParameterKnobStyle())
              }
            }
            GroupBox(label: Text("pickup")) {
              VStack {
                ParameterView(parameter: parameterTree.pickup.pickupTimbre)
                  .parameterStyle(ParameterKnobStyle())
                ParameterView(parameter: parameterTree.pickup.pickupVolume)
                  .parameterStyle(ParameterKnobStyle())
              }
            }
            GroupBox(label: Text("velocity")) {
              VStack {
                ParameterView(parameter: parameterTree.global.velocityCurve, name: "curve")
                  .parameterStyle(ParameterSymmetricKnobStyle())
                ParameterView(parameter: parameterTree.global.velocityDepth, name: "depth")
                  .parameterStyle(ParameterKnobStyle())
                  .curve(curve)
              }
            }
            GroupBox(label: Text(" ")) {
              VStack {
                ParameterView(parameter: parameterTree.global.volume)
                  .parameterStyle(ParameterKnobStyle())
                Rectangle().hidden()
              }
            }
              .groupBoxStyle(NoDividerGroupBoxStyle())
          }
          self.tuningView
            .onChange(of: selection) {
              if self.selection == 0 {
                // Disable tuning editing and note number update
                parameterTree.notenumber.updateNoteNumber.value = 0.0
              }
            }
        }
      }
      .groupBoxStyle(DividerGroupBoxStyle())
      .font(.system(size: geometry.size.height * _fontSizeScale))
    }
      .padding(5)
      .background(Color.controlBackgroundColor)
      .environment(\.colorScheme, .dark)
  }
}
