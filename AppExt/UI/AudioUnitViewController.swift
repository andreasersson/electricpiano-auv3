/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of electricpiano-auv3.
 *
 * electricpiano-auv3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * electricpiano-auv3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with electricpiano-auv3.  If not, see <https://www.gnu.org/licenses/>.
 */

import Combine
import CoreAudioKit
import SwiftUI

public class AudioUnitViewController: AUViewController, AUAudioUnitFactory {
  var audioUnit: AUAudioUnit?
  var messageChannel: AUMessageChannel?

  var hostingController: HostingController<ElectricPianoMainView>?

  private var observation: NSKeyValueObservation?

  let aspectRatio: Double = Double(ElectricPianoMainView.widthInUnits) / Double(ElectricPianoMainView.heightInUnits)

  public override var preferredContentSize: CGSize {
    get {return NSSize(width: 600, height: 600 / aspectRatio)}
    set { super.preferredContentSize = newValue }
  }

  open override var preferredMaximumSize: NSSize { return NSSize(width: 1000, height: 1000 / aspectRatio)}
  open override var preferredMinimumSize: NSSize { return NSSize(width: 300, height: 300 / aspectRatio)}

  deinit {}

  public func createAudioUnit(with componentDescription: AudioComponentDescription) throws -> AUAudioUnit {
    audioUnit = try ElectricPiano(componentDescription: componentDescription, options: [])
    guard let audioUnit = self.audioUnit as? ElectricPiano else {
      return audioUnit!
    }

    defer {
      // Configure the SwiftUI view after creating the AU to make sure that the parameter tree is set up.
      DispatchQueue.main.async {
        self.configureSwiftUIView(audioUnit: audioUnit)
      }
    }

    self.observation = audioUnit.observe(\.allParameterValues, options: [.new]) { _, _ in
      guard let tree = audioUnit.parameterTree else { return }
      for parameter in tree.allParameters { parameter.value = parameter.value }
    }

    return audioUnit
  }

  private func configureSwiftUIView(audioUnit: AUAudioUnit) {
    if let host = hostingController {
      host.removeFromParent()
      host.view.removeFromSuperview()
    }

    guard let observableParameterTree = audioUnit.observableParameterTree else {
      return
    }

    guard let epau = audioUnit as? ElectricPiano else {
      return
    }

    guard let tuning = epau.tuning else {
      return
    }

    let content = ElectricPianoMainView(parameterTree: observableParameterTree, tuning: tuning)

    let host = HostingController(rootView: content)
    self.addChild(host)
    host.view.frame = self.view.bounds
    self.view.addSubview(host.view)
    hostingController = host

    host.view.translatesAutoresizingMaskIntoConstraints = false
    host.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    host.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
    host.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    host.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    self.view.bringSubviewToFront(host.view)
  }
}
